{
  inputs,
  outputs,
  pkgs,
  lib,
  ...
}: {
  myHomeManager = {
    bundles.general.enable = true;
    bundles.desktop.enable = true;

    firefox.enable = true;
    hyprland.enable = true;

    monitors = [
      {
        name = "DP-1";
        width = 3840;
        height = 2160;
        refreshRate = 60.0;
        x = 0;
        y = 0;
      }
      {
        name = "DP-2";
        width = 3840;
        height = 2160;
        refreshRate = 60.0;
        x = 3840;
        y = 0;
      }
    ];
  };

  colorScheme = inputs.nix-colors.colorSchemes.catppuccin-mocha;

  home = {
    stateVersion = "23.11";
    homeDirectory = lib.mkDefault "/home/david";
    username = "david";

    packages = with pkgs; [
    ];
  };
}
