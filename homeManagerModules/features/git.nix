{
  config,
  lib,
pkgs,
  ...
}: {
  programs.git = {
    package = pkgs.gitAndTools.gitFull;
    enable = true;
    userName = "David Vogel";
    userEmail = "btwiue@proton.me";
    signing.key = "49C04CF3639FA1C5CB547E6CA0276D3B31F47B8B";
    extraConfig = {
      init.defaultBranch = "main";
      # core.editor = "nvim";
      # pull.rebase = "false";
      url = {
        "https://github.com/".insteadOf = "gh:";
        "https://codeberg.org/".insteadOf = "cb:";
        "https://gitlab.com/".insteadOf = "gl:";
      };
    };
  };

  programs.lazygit = {
    enable = true;
    settings = {
      gui = {
        showIcons = true;
        theme.lightTheme = false;
      };
      # os.editPreset = "emacsclient -cn";
      git = {
        paging.colorArg = "always";
        pager = "diff-so-fancy";
      };
    };
  };

  home.packages = with pkgs; [diff-so-fancy difftastic];
}
