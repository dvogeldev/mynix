{
  pkgs,
  inputs,
  config,
  ...
}: {
  programs.rbw = {
    enable = true;
    settings = {
      email = "dvogelca@proton.me";
      # lock_timeout = 300;
      pinentry = "curses";
    };
  };
}
